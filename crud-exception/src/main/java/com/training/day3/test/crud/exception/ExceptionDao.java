package com.training.day3.test.crud.exception;

import com.training.day3.test.crud.employee.UserNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

@RestController
@ControllerAdvice
public class ExceptionDao extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllException(Exception ex, WebRequest request){
        ExceptionModel exception = new ExceptionModel(request.getDescription
                (false),"Exception Occured",new Date());
        return new ResponseEntity(exception,HttpStatus.INTERNAL_SERVER_ERROR);
    }
    @ExceptionHandler(UserNotFoundException.class)
    public final ResponseEntity<Object> UserNotFoundException(Exception ex, WebRequest request){
        ExceptionModel exception = new ExceptionModel(request.getDescription
                (false),"Exception Occured",new Date());
        return new ResponseEntity(exception,HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
