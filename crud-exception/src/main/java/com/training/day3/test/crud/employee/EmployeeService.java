package com.training.day3.test.crud.employee;

import com.training.day3.test.crud.employee.EmployeeDao;
import com.training.day3.test.crud.employee.EmployeeModel;
import com.training.day3.test.crud.exception.ExceptionDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmployeeService {

    //endpoint
    @Autowired
    private EmployeeDao employeeDao;

    @Autowired
    private ExceptionDao exceptionDao;

    //read
    @GetMapping("/findAllEmployees")
    public List<EmployeeModel> findAll(){
        return employeeDao.findAll();
    }

    //retrieve one
    @GetMapping("/findEmployee/{id}")
    public EmployeeModel findOne(@PathVariable Integer id) {
        EmployeeModel empid = employeeDao.findOne(id);
        if(empid == null)
            throw new UserNotFoundException("Employee Id Not Found for " +id);
        return empid;
    }

    //update employee
    @PostMapping("/updateEmployee")
    public void updateEmployee(@RequestBody EmployeeModel newemp ){
        employeeDao.updateEmployee(newemp);
    }
}
