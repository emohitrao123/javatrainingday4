package com.training.day3.test.crud.employee;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class EmployeeDao {

    private static List<EmployeeModel> employee = new ArrayList<>();

    static {
        employee.add(new EmployeeModel(1,"Mohit", "Architect"));
        employee.add(new EmployeeModel(2,"Pratik", "Developer"));
        employee.add(new EmployeeModel(3,"Praveen", "Tester"));

    }

    public List<EmployeeModel> findAll(){
        return employee;
    }


    public EmployeeModel findOne(Integer id){
        for(EmployeeModel emp:employee) {
            if (emp.getEmpId() == id)
                return emp;
        }
        return null;
    }

   /* public EmployeeModel findOnBasisOfId(Integer id){
        return employee.stream().filter(i -> getId()
    }*/
   public void updateEmployee(EmployeeModel newemp){
           employee.add(newemp);

   }
}
