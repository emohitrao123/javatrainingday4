package com.training.day3.test.crud.exception;

import java.util.Date;

public class ExceptionModel {

    private String statusCode;

    private String message;

    private Date timestamp;

    public ExceptionModel(String statusCode, String message, Date timestamp) {
        this.statusCode = statusCode;
        this.message = message;
        this.timestamp = timestamp;
    }
}
